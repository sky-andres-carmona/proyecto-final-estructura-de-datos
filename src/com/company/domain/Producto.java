package com.company.domain;
import java.util.ArrayList;
import java.util.List;

public class Producto {
    public Integer codigo;
    public String modelo;
    public List<String> series;

    public Producto(Integer codigo, String modelo) {
        this.codigo = codigo;
        this.modelo = modelo;
        this.series = new ArrayList<>();
    }

    public Producto(Integer codigo, String modelo, String[] series) {
        this.codigo = codigo;
        this.modelo = modelo;
        this.series = new ArrayList<>();

        for (int i = 0; i < series.length; i++) {
            this.ingresarStock(series[i]);
        }
    }

    public void ingresarStock(String serie) {
        this.series.add(serie);
    }

    public void reducirStock(String serie) {
        List<String> nSeries =  new ArrayList<>();

        String[] series = this.series.toArray(new String[0]);

        for (int i = 0; i < series.length; i++) {
            if (!series[i].equals(serie)){
                nSeries.add(series[i]);
            }
        }

        this.series = nSeries;
    }
}
