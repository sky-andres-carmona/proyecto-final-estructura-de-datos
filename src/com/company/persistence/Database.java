package com.company.persistence;

import com.company.domain.Producto;

import java.util.ArrayList;
import java.util.List;

public class Database {
    public static Database shared = new Database();
    public List<Producto> productos;

    public Database(){
        productos = new ArrayList<>();

        productos.add(new Producto(1, "Retroexcavadora R1600G", new String[] {"G5400809", "GCT03091"} ));

        productos.add(new Producto(2, "Rodillo Liso Vibratorio CS54B", new String[] {"FRS02233"} ));

        productos.add(new Producto(3, "Tractor de Orugas D6T", new String[] {"2031792-77", "Z1A23459"} ));

        productos.add(new Producto(4, "Cargador de Ruedas 966L", new String[] {"KEL20214", "G5400839", "M5K04899"} ));

        productos.add(new Producto(5, "Excavadore Hidráulica 330G", new String[] {"M5K04939"} ));

        productos.add(new Producto(6, "Scoop UGM Cabina Cerrada R1600H", new String[] {"GDY20138"} ));

        productos.add(new Producto(7, "Scoop UGM Cabina Abierta R1300G", new String[] {"Z1A23459", "KEL20214"} ));

        productos.add(new Producto(8, "Cargador de ruedas 950GC", new String[] {"G5400839"} ));

        productos.add(new Producto(9, "Motoniveladora 149GC", new String[] {"M5K04899", "M5K04939"} ));

        productos.add(new Producto(10, "Motor Industrial C18", new String[] {"GDY20138", "TNS01371"} ));

        productos.add(new Producto(11, "Rodillo de Pavimentación CB10", new String[] {"G5400829", "FEK20270", "JFW20210"} ));
    }
}
