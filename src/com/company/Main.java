package com.company;

import java.lang.reflect.Array;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import com.company.domain.Producto;
import com.company.persistence.Database;
import javafx.util.Pair;

public class Main {
    static Scanner sc = new Scanner(System.in);
    static int comando_seleccionado;
    static boolean salir = false;

    public static String padRight(String s, int n) {
        return String.format("%-" + n + "s", s);
    }

    /*
     * Módulo de entrada
     * - Ingresar stock de producto por código de producto y a cada producto se le agrega el número de serie
     *
     * Módulo de búsqueda
     * - Debe permitirme buscar por código o mostrarme to-do lo disponible
     *
     * Módulo de salida
     * - Debe permitirme sacar stock ingresando el código de producto Y serie.
     * */
    public static void main(String[] args) {
        System.out.println("Bienvenido al sistema de control de kardex del almacén de la empresa Ferreyros.");
        System.out.println("-----------------------------o--------------------------------");
        System.out.println("Cargando datos...");
        cargar(2);
        System.out.println("Datos cargados...");
        System.out.println("-----------------------------o--------------------------------");

        do {
            comando_seleccionado = solicitarComando();

            switch (comando_seleccionado) {
                case 1:
                    verInventario();
                    break;
                case 2:
                    buscarPorCodigo();
                    break;
                case 3:
                    buscarPorSerie();
                    break;
                case 4:
                    ingresarStock();
                    break;
                case 5:
                    reducirStock();
                    break;
                case 6:
                    salir = true;
                    break;
                default:
                    continue;
            }
            cargar(2);
        } while (!salir);

        System.out.println("Cerrando el sistema.");
    }

    public static int solicitarComando() {
        System.out.println("Introduzca un comando:");
        System.out.println("1 - Ver inventario.");
        System.out.println("2 - Búsqueda por código.");
        System.out.println("3 - Búsqueda por número de serie.");
        System.out.println("4 - Ingresar stock.");
        System.out.println("5 - Reducir stock.");
        System.out.println("6 - Salir.");
        System.out.println("----------------------------------");
        return sc.nextInt();
    }

    public static void verInventario(){
        System.out.println("Se mostrará el inventario completo.");
        System.out.println("-----------------------o-------------------------");
        System.out.println("Cod. | " + padRight("Modelo", 40) + " | Stock");

        Database.shared.productos.forEach(producto -> {
            System.out.println(padRight(producto.codigo.toString(), 4) + " | " + padRight(producto.modelo, 40) + " | " + producto.series.size());
        });

        System.out.println("-----------------------o-------------------------");
    }

    public static void buscarPorCodigo(){
        Integer codigoProducto;
        System.out.println("Se realizará una búsqueda por código de producto.");
        System.out.println("-----------------------o-------------------------");

        System.out.println("Ingrese el código del producto buscado:");
        codigoProducto = sc.nextInt();

        Producto[] productos = Database.shared.productos.toArray(new Producto[0]);

        for (int i = 0; i < productos.length; i++) {
            if (productos[i].codigo == codigoProducto) {
                System.out.println("Cod. | " + padRight("Modelo", 40) + " | Stock");
                System.out.println(padRight(productos[i].codigo.toString(), 4) + " | " + padRight(productos[i].modelo, 40) + " | " + productos[i].series.size());

                String[] series = productos[i].series.toArray(new String[0]);
                if (series.length > 0) {
                    System.out.println("Números de serie disponibles");
                    for (int j = 0; j < series.length; j++) {
                        System.out.println("- " + series[j]);
                    }
                }
            }
        }
        System.out.println("-----------------------o-------------------------");
    }

    public static void buscarPorSerie(){
        String numeroDeSerie = "";

        System.out.println("Se realizará una búsqueda por número de serie de producto.");
        System.out.println("-----------------------o-------------------------");

        System.out.println("Ingrese el número de serie del producto buscado:");
        numeroDeSerie = sc.next();

        Producto[] productos = Database.shared.productos.toArray(new Producto[0]);

        for (int i = 0; i < productos.length; i++) {
            String[] series = productos[i].series.toArray(new String[0]);

            for (int j = 0; j < series.length; j++) {
                if (series[j].equals(numeroDeSerie)) {
                    System.out.println("Cod. | " + padRight("Modelo", 40) + " | Stock");
                    System.out.println(padRight(productos[i].codigo.toString(), 4) + " | " + padRight(productos[i].modelo, 40) + " | " + productos[i].series.size());
                }
            }
        }
        System.out.println("-----------------------o-------------------------");
    }

    public static void ingresarStock(){
        Producto productoIngresado = null;
        Integer codigoProducto;
        Integer stockEntrante;
        System.out.println("Se realizará un ingreso de stock.");
        System.out.println("-----------------------o-------------------------");

        System.out.println("Ingrese el código del producto buscado:");
        codigoProducto = sc.nextInt();

        Producto[] productos = Database.shared.productos.toArray(new Producto[0]);

        for (int i = 0; i < productos.length; i++) {
            if (productos[i].codigo == codigoProducto) {
                productoIngresado = productos[i];
            }
        }

        System.out.println("Ingrese el stock entrante:");
        stockEntrante = sc.nextInt();

        if (stockEntrante > 0) {
            for (int i = 0; i < stockEntrante; i++) {
                System.out.println("Ingrese el número de serie:");
                productoIngresado.ingresarStock(sc.next());
            }
        } else {
            System.out.println("No se ingresó stock.");
        }
    }

    public static void reducirStock(){
        Producto productoRetirado = null;
        String numeroDeSerie = "";

        System.out.println("Se realizará un retiro de stock de producto.");
        System.out.println("-----------------------o-------------------------");

        System.out.println("Ingrese el número de serie del producto a retirar:");
        numeroDeSerie = sc.next();

        Producto[] productos = Database.shared.productos.toArray(new Producto[0]);

        for (int i = 0; i < productos.length; i++) {
            String[] series = productos[i].series.toArray(new String[0]);

            for (int j = 0; j < series.length; j++) {
                if (series[j].equals(numeroDeSerie)) {
                    productoRetirado = productos[i];
                }
            }
        }

        productoRetirado.reducirStock(numeroDeSerie);

        System.out.println("Producto retirado del inventario.");
        System.out.println("-----------------------o-------------------------");
    }


    public static void cargar(int segundos) {
        try {
            TimeUnit.SECONDS.sleep(segundos);
        } catch (InterruptedException err){
            System.out.println(err.getMessage());
        }
    }
}
